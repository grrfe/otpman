FROM openjdk:11-slim-bullseye
ARG tag
COPY build/libs/otpman-$tag.jar /app.jar

VOLUME /database_dir

CMD ["java", "-jar", "app.jar", "config.json"]
