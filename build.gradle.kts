import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("com.gitlab.grrfe.version-helper:version-helper:2.1.23")
    }
}

plugins {
    kotlin("jvm") version "1.7.10"
    java
    application
    id("net.nemerosa.versioning") version "3.0.0"
}

group = "fe.otpman"

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
    mavenLocal()
}

dependencies {
    implementation("com.sun.mail:javax.mail:1.6.2")
    implementation("org.jsoup:jsoup:1.15.3")
    implementation("com.google.code.gson:gson:2.10")
    implementation("io.insert-koin:koin-core:3.2.2")

    implementation("com.gitlab.grrfe:koin-helper:2.0.3")
    implementation("com.gitlab.grrfe:GSONKtExtensions:2.1.2")
    implementation("com.gitlab.grrfe:tgbotutilkt:1.1.3")
    implementation("com.gitlab.grrfe:version-helper:2.1.1")
    implementation("com.gitlab.grrfe:sqlite-koin:1.0.2")

    implementation("io.github.microutils:kotlin-logging-jvm:2.0.11")
    implementation("ch.qos.logback:logback-classic:1.2.9")

    implementation("org.xerial:sqlite-jdbc:3.39.3.0")

    implementation("com.github.pengrad:java-telegram-bot-api:6.2.0")

    implementation("org.jetbrains.exposed:exposed-core:0.40.1")
    implementation("org.jetbrains.exposed:exposed-dao:0.40.1")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.40.1")
    implementation("org.jetbrains.exposed:exposed-java-time:0.40.1")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")

    testImplementation(kotlin("test"))
//    testImplementation("io.insert-koin:koin-test:3.2.0")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClass.set("fe.otpman.MainKt")
}

apply {
    this.plugin("com.gitlab.grrfe.version-helper")
}

