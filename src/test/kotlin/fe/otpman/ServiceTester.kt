package fe.otpman

import fe.gson.extensions.fileToInstance
import fe.koinhelper.config.configModules
import fe.otpman.config.Config
import fe.otpman.config.ServiceConfig
import org.jsoup.Jsoup
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.startKoin
import java.io.File

fun main() {
    startKoin {
        modules(
            configModules(
                gson.fileToInstance<Config>("config2.json"), mapOf(
                    "serviceConfig" to { it.serviceConfig },
                )
            )
        )
    }


    ServiceTester
}

object ServiceTester : KoinComponent {
    private val serviceConfig by inject<ServiceConfig>()
    private val test = mapOf(
        "BinanceLogin" to "binance_login.html"
    )

    init {
        test.forEach { (serviceName, sampleFile) ->
            val file = File("sample", sampleFile)
            val service = serviceConfig.services.find { it.name == serviceName }!!

            val doc = Jsoup.parse(file).body()
            val element = doc.selectXpath(service.xpath)

            println(element.text())
        }
    }
}
