package fe.otpman.config

import fe.koinhelper.sqlite.config.SqliteConfig
import fe.tgbotutil.config.TelegramConfig

data class Config(
    val serviceConfig: ServiceConfig,
    val telegramConfig: TelegramConfig,
    val sqliteConfig: SqliteConfig,
)
