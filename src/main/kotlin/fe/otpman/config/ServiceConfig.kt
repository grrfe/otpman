package fe.otpman.config

import fe.koinhelper.config.ConfigModule
import javax.mail.search.SearchTerm

data class ServiceConfig(
    val services: List<Service>
) {
    fun findServiceByName(name: String) = this.services.find { it.name.equals(name, ignoreCase = true) }
}

data class Service(
    val name: String,
    val searchTerm: SearchTerm,
    val xpath: String
)
