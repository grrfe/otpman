package fe.otpman.module.database.entity

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable

object TelegramUsers : IdTable<Long>("users") {
    override val id = long("userId").entityId()

    val lastChatId = long("lastChatId")

    override val primaryKey = PrimaryKey(id)
}


class TelegramUser(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<TelegramUser>(TelegramUsers)

    var userId by TelegramUsers.id
    var lastChatId by TelegramUsers.lastChatId
}

