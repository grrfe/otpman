package fe.otpman.module.database.entity

import fe.otpman.module.mail.MailSession
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object Mailboxes : IntIdTable("mailboxes") {
    val belongsTo = reference("belongsTo", TelegramUsers)

    val folder = varchar("folder", 256)
    val host = varchar("host", 256)
    val port = integer("port")
    val username = varchar("username", 256)
    val password = varchar("password", 256)
}

class Mailbox(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Mailbox>(Mailboxes)

    var belongsTo by Mailboxes.belongsTo
    var folder by Mailboxes.folder
    var host by Mailboxes.host
    var port by Mailboxes.port
    var username by Mailboxes.username
    var password by Mailboxes.password

    var services by Service via MailboxServices

    val mailSession by lazy {
        MailSession(this.folder, this.host, this.port, this.username, this.password).also {
            it.connect()
        }
    }

    override fun toString(): String {
        return "Mailbox(belongsTo=$belongsTo, folder='$folder', host='$host', port=$port, username='$username', password='$password', services=$services)"
    }
}
