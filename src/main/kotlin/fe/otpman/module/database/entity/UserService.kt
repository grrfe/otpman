package fe.otpman.module.database.entity

import org.jetbrains.exposed.sql.Table

object MailboxServices : Table("mailbox_services") {
    val mailbox = reference("mailbox", Mailboxes)
    val service = reference("service", Services)

    override val primaryKey = PrimaryKey(mailbox, service)
}
