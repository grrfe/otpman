package fe.otpman.module.database.entity

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object Services : IntIdTable("services") {
    val name = varchar("name", 256)
}

class Service(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Service>(Services)

    var name by Services.name
}
