package fe.otpman.module.mail

import com.pengrad.telegrambot.TelegramBot
import com.sun.mail.imap.IdleManager
import fe.otpman.config.Service
import fe.otpman.config.ServiceConfig
import fe.otpman.ext.getText
import fe.otpman.ext.transactionHandleException
import fe.otpman.module.database.entity.*
import fe.tgbotutil.buildMarkdown
import fe.tgbotutil.ext.sendMarkdownV2
import mu.KotlinLogging
import org.jetbrains.exposed.sql.Database
import org.jsoup.Jsoup
import org.koin.dsl.module
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors
import javax.mail.Folder
import javax.mail.Message
import javax.mail.Session
import javax.mail.event.MessageCountEvent
import javax.mail.event.MessageCountListener

private val inboxReaderLogger = KotlinLogging.logger("InboxReaderModule")

val inboxReaderModule = module {
    single {
        val configServices = get<ServiceConfig>()

        val list = get<Database>().transactionHandleException {
            Mailbox.all().map { mailbox ->
                val services = mailbox.services.map { service ->
                    configServices.findServiceByName(service.name)
                        ?: error("Service ${service.name} not found in config!")
                }

                inboxReaderLogger.info("Found ${mailbox.username} which belongs to ${mailbox.belongsTo.value}")
                UserConfigHolder(mailbox.mailSession, mailbox.belongsTo.value, services)
            }
        } ?: emptyList()

        InboxReader(list, get())
    }
}

data class UserConfigHolder(
    val mailSession: MailSession,
    val userId: Long,
    val services: List<Service>,
)

class InboxReader(
    private val userConfigHolders: List<UserConfigHolder>,
    private val telegramBot: TelegramBot,
) {
    class InboxMessageListener(
        private val userConfigHolder: UserConfigHolder,
        private val telegramBot: TelegramBot,
        private val idleManager: IdleManager
    ) : MessageCountListener {
        companion object {
            private val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        }

        override fun messagesAdded(ev: MessageCountEvent) {
            findMatch(ev.messages, userConfigHolder.services)?.let { (message, service) ->
                val token = message.getText()?.let { Jsoup.parse(it).selectXpath(service.xpath).text() }
                val msg = if (!token.isNullOrEmpty()) buildMarkdown {
                    spaced(
                        { text("You have received a token from") },
                        { bold(service.name) },
                        { text("at") },
                        {
                            bold(dateFormat.format(message.sentDate))
                            bold(":")
                        },
                        { code(token) }
                    )
                } else buildMarkdown {
                    spaced(
                        { text("We found a mail from") },
                        { bold(service.name) },
                        { text("sent at") },
                        { bold(dateFormat.format(message.sentDate)) },
                        { text("but we failed to extract a token!") }
                    )
                }

                telegramBot.sendMarkdownV2(userConfigHolder.userId, msg)
            }

            kotlin.runCatching {
                idleManager.watch(ev.source as Folder)
            }
        }

        override fun messagesRemoved(e: MessageCountEvent) {
        }
    }

    fun start() {
        val executor = Executors.newCachedThreadPool()
        val emptySession = Session.getDefaultInstance(Properties())
        val idleManager = IdleManager(emptySession, executor)

        for (userConfig in userConfigHolders) {
            val folder = userConfig.mailSession.connect().inbox()

            folder.open(Folder.READ_WRITE)
            folder.addMessageCountListener(InboxMessageListener(userConfig, telegramBot, idleManager))

            kotlin.runCatching {
                idleManager.watch(folder)
                inboxReaderLogger.info("Watching folder ${folder.name} on account ${userConfig.userId}..")
            }.getOrElse {
                inboxReaderLogger.error("Watching folder ${folder.name} failed with ${it.message}")
            }
        }
    }
}


fun findMatch(messages: Array<Message>, services: List<Service>): Pair<Message, Service>? {
    messages.forEach { message ->
        services.forEach { service ->
            val match = message.match(service.searchTerm)
            if (match) {
                inboxReaderLogger.info("$service can handle $message")
                return message to service
            }
        }
    }

    return null
}
