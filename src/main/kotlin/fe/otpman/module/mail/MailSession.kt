package fe.otpman.module.mail

import java.util.*
import javax.mail.Folder
import javax.mail.Session
import javax.mail.Store

data class MailSession(
    val folder: String = "inbox",
    val host: String,
    val port: Int,
    val username: String,
    val password: String,
) {
    private val props = Properties().apply {
        this["mail.imap.ssl.enable"] = "true"
        this["mail.imap.usesocketchannels"] = "true"
    }

    private val session: Session = Session.getDefaultInstance(props)
    private val store: Store = session.getStore("imap")

    fun connect(): MailSession {
        if (!store.isConnected) store.connect(host, port, username, password)
        return this
    }

    fun inbox(): Folder = store.getFolder(folder)
}
