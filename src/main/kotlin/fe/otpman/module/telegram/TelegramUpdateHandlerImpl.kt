package fe.otpman.module.telegram

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.CallbackQuery
import com.pengrad.telegrambot.model.Message
import fe.tgbotutil.TelegramUpdateHandler
import fe.tgbotutil.command.CommandHandler
import org.koin.dsl.module

val telegramUpdateHandlerModule = module { single<TelegramUpdateHandler> { TelegramUpdateHandlerImpl(get()) } }


class TelegramUpdateHandlerImpl(bot: TelegramBot) : TelegramUpdateHandler(bot, CommandHandler(listOf())) {

    override fun handleProcessException(exception: Exception): Int? {
        exception.printStackTrace()
        return null
    }

    override fun messageCommandExecuted(message: Message, returnMsg: String?) {

    }

    override fun noCallbackHandlerFound(query: CallbackQuery) {
        println("No callback found $query")
    }
}
