package fe.otpman.util.term

import javax.mail.Message
import javax.mail.internet.InternetAddress
import javax.mail.search.SearchTerm

class FromRegexTerm(private val regex: Regex) : SearchTerm() {

    override fun match(msg: Message): Boolean {
        kotlin.runCatching { msg.from }.getOrNull()
            ?.map { a -> if (a is InternetAddress) a.toUnicodeString() else a.toString() }
            ?.map { a ->
                val startReplaced = with(a) {
                    if (this.startsWith("<")) this.substring(1) else this
                }

                with(startReplaced) {
                    if (this.endsWith(">")) this.substring(0, this.length - 1) else this
                }
            }
            ?.forEach { mail ->
                if (mail.matches(regex)) {
                    return true
                }
            }

        return false
    }


    override fun hashCode(): Int {
        return regex.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FromRegexTerm

        if (regex != other.regex) return false

        return true
    }
}
