package fe.otpman.util.term

import javax.mail.Message
import javax.mail.internet.InternetAddress
import javax.mail.search.SearchTerm

class SubjectRegexTerm(private val regex: Regex) : SearchTerm() {

    override fun match(msg: Message): Boolean {
        return kotlin.runCatching { msg.subject }.getOrNull()?.matches(regex) ?: false
    }


    override fun hashCode(): Int {
        return regex.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SubjectRegexTerm

        if (regex != other.regex) return false

        return true
    }
}
