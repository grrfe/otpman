package fe.otpman.util

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import fe.otpman.util.term.FromRegexTerm
import fe.otpman.util.term.SubjectRegexTerm
import java.lang.reflect.Type
import javax.mail.search.*

object SearchTermDeserializer : JsonDeserializer<SearchTerm> {
    private val searchTermTypeToken = object : TypeToken<List<SearchTerm>>() {}.type

    private inline fun <reified T : SearchTerm> JsonObject.deserializeChildren(context: JsonDeserializationContext): Array<T> {
        return (context.deserialize<T>(getAsJsonArray("children"), searchTermTypeToken) as List<T>).toTypedArray()
    }

    private fun JsonObject.valueAsString() = getAsJsonPrimitive("value").asString

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): SearchTerm? {
        val obj = json.asJsonObject
        return when (obj.getAsJsonPrimitive("type").asString) {
            "_and" -> {
                AndTerm(obj.deserializeChildren(context))
            }

            "_or" -> {
                OrTerm(obj.deserializeChildren(context))
            }

            "from" -> {
                FromStringTerm(obj.valueAsString())
            }

            "from_regex" -> {
                FromRegexTerm(Regex(obj.valueAsString()))
            }

            "subject" -> {
                SubjectTerm(obj.valueAsString())
            }

            "subject_regex" -> {
                SubjectRegexTerm(Regex(obj.valueAsString()))
            }

            else -> null
        }

    }
}
