package fe.otpman.init

import fe.koinhelper.ext.KoinInit
import fe.otpman.ext.transactionHandleException
import fe.otpman.module.database.entity.Mailboxes
import fe.otpman.module.database.entity.Services
import fe.otpman.module.database.entity.TelegramUsers
import fe.otpman.module.database.entity.MailboxServices
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.koin.core.component.inject

object DatabaseInit : KoinInit {
    private val database by inject<Database>()

    override fun init() {
        database.transactionHandleException {
            SchemaUtils.createMissingTablesAndColumns(TelegramUsers, Mailboxes, Services, MailboxServices)
        }
    }
}

