package fe.otpman.init

import fe.koinhelper.ext.KoinInit
import fe.otpman.module.mail.InboxReader
import org.koin.core.component.inject

object InboxReaderInit : KoinInit {
    private val inboxReader by inject<InboxReader>()

    override fun init() {
        inboxReader.start()
    }
}
