package fe.otpman.init

import fe.koinhelper.ext.KoinInit
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.system.exitProcess

object RestartInit : KoinInit {
    override fun init() {
        val pool = Executors.newScheduledThreadPool(1)
        pool.scheduleAtFixedRate({ exitProcess(1) }, 12, 12, TimeUnit.HOURS)
    }
}

