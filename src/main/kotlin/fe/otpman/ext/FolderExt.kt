package fe.otpman.ext

import fe.otpman.config.Service
import javax.mail.Flags
import javax.mail.Folder
import javax.mail.Message
import javax.mail.MessagingException

fun Folder.checkMessages(services: List<Service>): MutableMap<Message, Service>? {
    println("\tcheck new messages")
    return if (this.hasNewMessages()) {
        this.open(Folder.READ_WRITE)
        val map = searchRecent(services)

        map.forEach { (msg, _) ->
            msg.setFlag(Flags.Flag.SEEN, true)
        }

        this.close()
        map
    } else null
}

fun Folder.searchRecent(services: List<Service>): MutableMap<Message, Service> {
    val matches = mutableMapOf<Message, Service>()
    messages.reversed().forEach { msg ->
        if (msg.isSet(Flags.Flag.RECENT)) {
            services.forEach {
                runCatching {
                    if (msg.match(it.searchTerm)) {
                        matches[msg] = it
                    }
                }
            }
        } else {
            return matches
        }
    }

    return matches
}
