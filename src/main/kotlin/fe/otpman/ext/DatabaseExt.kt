package fe.otpman.ext

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Transaction

@Throws(Exception::class)
fun <T> Database.transactionRaw(statement: Transaction.() -> T): T {
    return org.jetbrains.exposed.sql.transactions.transaction(this, statement)
}

fun <T> Database.transactionHandleException(statement: (Transaction) -> T?): T? {
    return try {
        org.jetbrains.exposed.sql.transactions.transaction(this, statement)
    } catch (e: java.lang.Exception) {
        e.printStackTrace()
        null
    }
}
