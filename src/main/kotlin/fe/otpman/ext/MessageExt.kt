package fe.otpman.ext

import java.io.IOException
import javax.mail.MessagingException
import javax.mail.Multipart
import javax.mail.Part

//taken from https://www.oracle.com/technetwork/java/javamail/faq/index.html#mainbody
@Throws(MessagingException::class, IOException::class)
fun Part.getText(): String? {
    var textIsHtml = false

    if (this.isMimeType("text/*")) {
        val s = this.content as String
        textIsHtml = this.isMimeType("text/html")
        return s
    }
    if (this.isMimeType("multipart/alternative")) {
        // prefer html text over plain text
        val mp = this.content as Multipart
        var text: String? = null
        for (i in 0 until mp.count) {
            val bp: Part = mp.getBodyPart(i)
            if (bp.isMimeType("text/plain")) {
                if (text == null) text = bp.getText()
                continue
            } else if (bp.isMimeType("text/html")) {
                val s = bp.getText()
                if (s != null) return s
            } else {
                return bp.getText()
            }
        }

        return text
    } else if (this.isMimeType("multipart/*")) {
        val mp = this.content as Multipart
        for (i in 0 until mp.count) {
            val s = mp.getBodyPart(i).getText()
            if (s != null) return s
        }
    }
    return null
}
