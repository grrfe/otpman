package fe.otpman

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fe.gson.extensions.fileToInstance
import fe.koinhelper.config.configModules
import fe.koinhelper.ext.init
import fe.koinhelper.sqlite.module.sqlite.sqliteModule
import fe.otpman.config.Config
import fe.otpman.init.DatabaseInit
import fe.otpman.init.InboxReaderInit
import fe.otpman.init.RestartInit
import fe.otpman.init.TelegramUpdateHandlerInit
import fe.otpman.module.mail.inboxReaderModule
import fe.otpman.module.telegram.telegramUpdateHandlerModule
import fe.otpman.util.SearchTermDeserializer
import fe.tgbotutil.module.telegramBotModule
import fe.version.helper.Version.Companion.readVersion
import mu.KotlinLogging
import org.koin.core.context.GlobalContext.startKoin
import javax.mail.search.SearchTerm

val gson: Gson = GsonBuilder()
    .registerTypeAdapter(SearchTerm::class.java, SearchTermDeserializer)
    .create()

private val logger = KotlinLogging.logger("Main")

fun main(args: Array<String>) {
    if (args.isEmpty()) error("Please provide path to config!")

    val version = readVersion()
    logger.info("Launching ${version.full}")

    startKoin {
        modules(configModules(gson.fileToInstance<Config>(args[0]), mapOf(
            "config" to { it },
            "serviceConfig" to { it.serviceConfig },
            "telegramConfig" to { it.telegramConfig },
            "sqliteConfig" to { it.sqliteConfig }
        )))


        modules(
            sqliteModule,
            inboxReaderModule,
            telegramBotModule,
            telegramUpdateHandlerModule,
        )

        init(
            DatabaseInit,
            TelegramUpdateHandlerInit,
            InboxReaderInit,
            RestartInit
        )
    }
}
